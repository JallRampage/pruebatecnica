<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Ticket;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $path = 'challenge_dataset.csv';
        $data = Excel::toArray([], storage_path('app/' . $path));
        //saltarse la fila de las cabeceras;
        $data = array_slice($data[0], 1);

        foreach ($data as $row) {
            Ticket::create([
                'origin' => $row[0],
                'destination' => $row[1],
                'airline' => $row[2],
                'flight_num' => $row[3],
                'origin_iata_code' => $row[4],
                'origin_name' => $row[5],
                'origin_latitude' => $row[6],
                'origin_longitude' => $row[7],
                'destination_iata_code' => $row[8],
                'destination_name' => $row[9],
                'destination_latitude' => $row[10],
                'destination_longitude' => $row[11],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
