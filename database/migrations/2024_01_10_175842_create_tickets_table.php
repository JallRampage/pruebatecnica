<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id('ticket_id');
            $table->string('origin');
            $table->string('destination');
            $table->string('airline');
            $table->string('flight_num');
            $table->string('origin_iata_code');
            $table->string('origin_name');
            $table->string('origin_latitude');
            $table->string('origin_longitude');
            $table->string('destination_iata_code');
            $table->string('destination_name');
            $table->string('destination_latitude');
            $table->string('destination_longitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
