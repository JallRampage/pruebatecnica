<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;
use App\Models\Ticket;

class PDFController extends Controller
{
    public function generatePDF(Request $request)
    {
        $ticket=Ticket::find($request->ticket_id);
        //buscar en cache
        $dataOrigen=Cache::get($ticket->origin);
        $dataDestino=Cache::get($ticket->destination);

        $origenLatitud=$ticket['origin_latitude'];
        $origenLongitud=$ticket['origin_longitude'];

        $destinoLatitud=$ticket['destination_latitude'];
        $destinoLongitud=$ticket['destination_longitude'];

        $app_id='e97e26b4';
        $app_key='8b1620d9ca6492a89b0061f1727b49e9';

        $urlOrigen = 'http://api.weatherunlocked.com/api/current/' . $origenLatitud . ',' . $origenLongitud . '?lang=es&app_id=' . $app_id . '&app_key=' . $app_key;
        $urlDestino = 'http://api.weatherunlocked.com/api/current/' . $destinoLatitud . ',' . $destinoLongitud . '?lang=es&app_id=' . $app_id . '&app_key=' . $app_key;

        $responses = Http::pool(function (Pool $pool) use ($urlOrigen, $urlDestino, $dataOrigen, $dataDestino, $ticket) {
            if($dataOrigen==null){
                $pool->get($urlOrigen);
            }
            if($dataDestino==null){
                $pool->get($urlDestino);
            }
        });

        foreach ($responses as $response) {
            $effectiveUrl = $response->effectiveUri();
        
            if (!$dataOrigen && $effectiveUrl == $urlOrigen) {
                $dataOrigen = $response->json();
                Cache::put($ticket->origin, $dataOrigen, 180);
            }
        
            if (!$dataDestino && $effectiveUrl == $urlDestino) {
                $dataDestino = $response->json();
                Cache::put($ticket->destination, $dataDestino, 180);
            }
        }
        // Verificar si hubo un error en alguna de las solicitudes
        foreach ($responses as $response) {
            if ($response->failed()) {
                return;
            }
        }
        $data = [
            'title' => 'Reporte PDF',
            'ticket' => $ticket,
            'dataOrigen'=>$dataOrigen,
            'dataDestino'=>$dataDestino,
        ];

        $pdf = PDF::loadView('informe', $data);

        return $pdf->download('Reporte_'.$ticket->ticket_id.'.pdf');
    }
}
