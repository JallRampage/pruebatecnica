<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $table ='tickets';
    protected $primaryKey ='ticket_id';
    public $incrementing = True;

    protected $fillable = ['origin','destination','airline','flight_num','origin_iata_code','origin_name',
    'origin_latitude','origin_longitude','destination_iata_code','destination_name','destination_latitude',
    'destination_longitude'];

}
