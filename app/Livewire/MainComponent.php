<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Ticket;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Client\Pool;
use Livewire\WithPagination;
use Barryvdh\DomPDF\Facade\PDF;

class MainComponent extends Component
{
    use WithPagination;

    public $origenes;
    public $destinos;
    public $selector_origen;
    public $selector_destino;
    public $error;
    public $search;

    public $dataOrigen;
    public $dataDestino;

    public function mount(){
        $this->origenes = Ticket::distinct('origin')->pluck('origin');
        $this->selector_origen=$this->origenes[0];
        $this->destinos = Ticket::distinct('destination')->pluck('destination');
        $this->selector_destino =$this->destinos[0];  
    }

    public function render()
    {
        $tickets = Ticket::where('origin', $this->selector_origen)
        ->where('destination', $this->selector_destino)
        ->where('flight_num', 'like', '%'.$this->search.'%') 
        ->paginate(10);

        $this->dataOrigen = Cache::get($this->selector_origen);
        $this->dataDestino = Cache::get($this->selector_destino);

        $this->dataDestino = $this->dataDestino ?? null;
        $this->dataOrigen = $this->dataOrigen ?? null;

        return view('livewire.main-component',['tickets' => $tickets]);
    }

    
    public function fetchDataFromApi($data)
    {
        $this->error = null;
        $tickets=$data['data'];

        if ($tickets == null) {
            $this->error='No se encotraron datos';
            return;
        }

        $origenLatitud=$tickets[0]['origin_latitude'];
        $origenLongitud=$tickets[0]['origin_longitude'];

        $destinoLatitud=$tickets[0]['destination_latitude'];
        $destinoLongitud=$tickets[0]['destination_longitude'];

        $app_id='e97e26b4';
        $app_key='8b1620d9ca6492a89b0061f1727b49e9';

        $urlOrigen = 'http://api.weatherunlocked.com/api/current/' . $origenLatitud . ',' . $origenLongitud . '?lang=es&app_id=' . $app_id . '&app_key=' . $app_key;
        $urlDestino = 'http://api.weatherunlocked.com/api/current/' . $destinoLatitud . ',' . $destinoLongitud . '?lang=es&app_id=' . $app_id . '&app_key=' . $app_key;

        //Comprobar si la informacion sigue en cache
        $this->dataOrigen = Cache::get($this->selector_origen);
        $this->dataDestino = Cache::get($this->selector_destino);

        // manejo de concurencia
        $responses = Http::pool(function (Pool $pool) use ($urlOrigen, $urlDestino) {
            if($this->dataOrigen==null){
                $pool->get($urlOrigen);
            }
            if($this->dataDestino==null){
                $pool->get($urlDestino);
            }
        });

        foreach ($responses as $response) {
            $effectiveUrl = $response->effectiveUri();
        
            if (!$this->dataOrigen && $effectiveUrl == $urlOrigen) {
                $this->dataOrigen = $response->json();
                Cache::put($this->selector_origen, $this->dataOrigen, 180);
            }
        
            if (!$this->dataDestino && $effectiveUrl == $urlDestino) {
                $this->dataDestino = $response->json();
                Cache::put($this->selector_destino, $this->dataDestino, 180);
            }
        }
        // Verificar si hubo un error en alguna de las solicitudes
        foreach ($responses as $response) {
            if ($response->failed()) {
                $this->error = 'Error en la solicitud: ' . $response->status();
                return;
            }
        }
    }

    public function doSearch(){}

}
