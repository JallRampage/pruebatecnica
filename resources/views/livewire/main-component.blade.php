<div>
    <div class="mt-4">
        <span class="bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700">Origen :</span>
        <select wire:model.change='selector_origen' name="selector_origen" class="border inline-block p-2 mb-2 border-gray-400 rounded-full h-10 w-52">
            @foreach ($origenes as $value)
                <option value="{{$value}}" >{{$value}}</option>
            @endforeach
        </select>
        <br>
            @if ($destinos != null)
                <div class="mb-4">
                    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700">Destino :</span>
                    <select wire:model.change="selector_destino" name="selector_destino" class="border border-gray-400 inline-block p-2 rounded-full h-10 w-52">
                        @foreach ($destinos as $destino)
                            <option value="{{ $destino }}">{{ $destino }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="flex w-full ">
                    <div class="w-1/2">
                        <span class="bg-gray-200 rounded-full px-3 py-1  text-sm font-semibold text-gray-700">Num Vuelo :</span>
                        <input type="text" wire:keydown='doSearch' wire:model='search' class="border border-gray-500">    
                    </div>
                    <div class="flex justify-end w-1/2">
                        <button wire:click.prevent="fetchDataFromApi({{json_encode($tickets)}})" type="button" class="bg-blue-500 text-white px-4 py-2 ml-12 rounded-full">Actualizar <i class="fa-solid fa-cloud-moon"></i></button>
                    </div>
            </div>
            
    </div>
    <div class="flex w-full bg-red-300 mt-2 rounded-full">
        @if ($error)
            <div class="alert alert-danger text-white px-2 py-1">
                {{ $error }}
            </div>
        @endif
    </div>
    <table class="w-full text-sm mt-4">
        <thead class="">
            <tr class=" sticky top-0 bg-blue-900 text-white">
                <th class="text-center">Origen</th>
                <th class="text-center">Destino</th>
                <th class="text-center">Aerolinea</th>
                <th class="text-center">Num Vuelo</th>
                <th class="text-center">Aeropuerto origen</th>
                <th class="text-center"
                    >Clima
                    <i class="fa-solid fa-cloud-sun"></i>
                </th>
                <th class="text-center">Aeropuerto destino</th>
                <th class="text-center"
                    >Clima
                    <i class="fa-solid fa-cloud-sun"></i>
                </th>
                <th>
                    <i class="fa-solid fa-circle-arrow-down"></i>
                </th>
            </tr>
        </thead>
        <tbody wire:loading.remove>
            @if ($tickets!=null)
                @foreach ($tickets as $t)
                <tr class="border-b border-gray-200 hover:bg-gray-50">
                    <td class="p-2 ml-1 mr-1 text-center ">{{ $t->origin }}</td>
                    <td class="p-2 ml-1 mr-1 text-center">{{ $t->destination }}</td>
                    <td class="p-2 ml-1 mr-1 text-center">{{ $t->airline }}</td>
                    <td class="p-2 ml-1 mr-1 text-center">{{ $t->flight_num }}</td>
                    <td class="p-2 ml-1 mr-1 text-center">{{ $t->origin_name }}</td>
                    @if ($dataOrigen!=null)
                    <td class="p-2 ml-1 mr-1 text-center">
                        <div class="flex">
                            {{$dataOrigen['wx_desc']}}
                            <img src="{{ asset('images/'.$dataOrigen['wx_icon']) }}">
                            {{$dataOrigen['temp_c']}}°C
                        </div>
                    </td>
                    @else
                    <td class="p-2 ml-1 mr-1 text-center">{{'-'}}</td>
                    @endif
                    <td class="p-2 ml-1 mr-1 text-center">{{ $t->destination_name }}</td>
                    @if ($dataDestino!=null)
                        <td class="p-2 ml-1 mr-1 text-center">
                            <div class="flex">
                                {{$dataDestino['wx_desc']}}
                                <img src="{{ asset('images/'.$dataDestino['wx_icon']) }}">
                                {{$dataDestino['temp_c']}}°C
                            </div>
                        </td>
                    @else
                        <td class="p-2 ml-1 mr-1 text-center">{{'-'}}</td>
                    @endif
                    <td class="p-2 ml-1 mr-1 text-center"> 
                        <form action="{{ route('generate-pdf') }}" method="POST">
                            @csrf
                            <input type="hidden" name="ticket_id" value="{{$t->ticket_id}}">
                            <button type="submit" ><i class="fa-solid fa-download"></i></button>
                        </form>
                        
                    </td>
                </tr>
                @endforeach
            @endif
        </tbody>

    </table>
    <div class="flex items-center justify-center h-3/4">
            <div wire:loading wire:target='selector_origen'>
                <div class="flex items-center mt-48">
                    <svg aria-hidden="true" class="mr-2 w-8 h-16 text-gray-200 animate-spin dark:text-gray-600 fill-blue-900" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>
                    <p> Cargando...</p>
                </div>
            </div>
            <div wire:loading wire:target='selector_destino'>
                <div class="flex items-center mt-48">
                    <svg aria-hidden="true" class="mr-2 w-8 h-16 text-gray-200 animate-spin dark:text-gray-600 fill-blue-900" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>
                    <p> Cargando...</p>
                </div>
            </div>
            <div wire:loading wire:target='search'>
                <div class="flex items-center mt-48">
                    <svg aria-hidden="true" class="mr-2 w-8 h-16 text-gray-200 animate-spin dark:text-gray-600 fill-red-900" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
                    </svg>
                    <p> Buscando Coincidencias...</p>
                </div>
            </div>
            <div wire:loading wire:target='fetchDataFromApi'>
                <div class="flex items-center mt-48">
                    <p> <i class="fa-solid fa-cloud-showers-heavy fa-xl mr-2" style="color: #74C0FC;"></i> Obteniendo el Clima ...</p>
                </div>
            </div>
    </div>
    @if ($tickets!=null)
        <div class="pt-4 flex justify-end">
            {{ $tickets->links() }}
        </div>
    @endif        

</div>
