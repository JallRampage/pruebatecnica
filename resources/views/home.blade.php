<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        @vite('resources/css/app.css')
        <script src="https://kit.fontawesome.com/9f0f52e542.js" crossorigin="anonymous"></script>
    </head>
    <body class="w-full flex justify-center">
        <x-app-layout/>
        <div class="relative w-full mt-20 p-4 sticky top-0">
            @livewire('main-component')
        </div>
    </body>
</html>

