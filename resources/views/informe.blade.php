<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite('resources/css/app.css')
    <title>Reporte Completo del Clima</title>
</head>
<body class="bg-gray-100 p-8">
    <div class="max-w-2xl mx-auto bg-white p-8 rounded-md shadow-md">
        <div class="mb-4 text-center">
            <img src="{{ asset('images/DL_logo.PNG') }}" style="width:50%">
        </div>
        <div class="mb-4">
            <h2 class="text-2xl font-semibold mb-2">Datos del Ticket No. {{$ticket->ticket_id}}</h2>
            <div class="grid grid-cols-2 gap-4">
                <div class="flex">
                    <strong>Origen:</strong> {{ $ticket->origin }}
                </div>
                <div>
                    <strong>Destino:</strong> {{ $ticket->destination }}
                </div>
                <div>
                    <strong>Número de Vuelo:</strong> {{ $ticket->flight_num }}
                </div>
                <div>
                    <strong>Aerolínea:</strong> {{ $ticket->airline }}
                </div>
            </div>
        </div>

        <div class="mb-4">
            <h2 class="text-2xl font-semibold mb-2">Información del Aeropuerto - Origen <img src="{{ asset('images/'.$dataOrigen['wx_icon']) }}" style="width: 5%"></h2>
            <div>
                <strong>Aeropuerto:</strong> {{ $ticket->origin_name }}
            </div>
            <div class="mt-2">
                <strong>Datos del Clima:</strong>
                <ul>
                    <li>
                        <strong>Descripción:</strong> {{ $dataOrigen['wx_desc'] }}
                    </li>
                    <li><strong>Temperatura :</strong> {{ $dataOrigen['temp_c'] }} °C</li>
                    <li><strong>Sensación Térmica :</strong> {{ $dataOrigen['feelslike_c'] }} °C</li>
                    <li><strong>Humedad :</strong> {{ $dataOrigen['humid_pct'] }} %</li>
                    <li><strong>Velocidad del Viento :</strong> {{ $dataOrigen['windspd_kmh'] }} km/h</li>
                    <li><strong>Dirección del Viento:</strong> {{ $dataOrigen['winddir_compass'] }}</li>
                    <li><strong>Nubosidad :</strong> {{ $dataOrigen['cloudtotal_pct'] }} %</li>
                    <li><strong>Visibilidad :</strong> {{ $dataOrigen['vis_km'] }} km</li>
                    <li><strong>Presión Atmosférica :</strong> {{ $dataOrigen['slp_mb'] }} mb</li>
                </ul>
            </div>
        </div>

        <div class="mb-4">
            <h2 class="text-2xl font-semibold mb-2">Información del Aeropuerto - Destino <img src="{{ asset('images/'.$dataDestino['wx_icon']) }}" style="width: 5%"></h2>
            <div>
                <strong>Aeropuerto:</strong> {{ $ticket->destination_name }}
            </div>
            <div class="mt-2">
                <strong>Datos del Clima:</strong>
                <ul>
                    <li>
                        <strong>Descripción:</strong> {{ $dataDestino['wx_desc'] }}
                    </li>
                    <li><strong>Temperatura:</strong> {{ $dataDestino['temp_c'] }} °C</li>
                    <li><strong>Sensación Térmica :</strong> {{ $dataDestino['feelslike_c'] }} °C</li>
                    <li><strong>Humedad :</strong> {{ $dataDestino['humid_pct'] }} %</li>
                    <li><strong>Velocidad del Viento :</strong> {{ $dataDestino['windspd_kmh'] }} km/h</li>
                    <li><strong>Dirección del Viento:</strong> {{ $dataDestino['winddir_compass'] }}</li>
                    <li><strong>Nubosidad :</strong> {{ $dataDestino['cloudtotal_pct'] }} %</li>
                    <li><strong>Visibilidad :</strong> {{ $dataDestino['vis_km'] }} km</li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
